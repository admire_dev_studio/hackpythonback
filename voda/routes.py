#!/home/c/cp36696/myenv/bin/python3
from flask import Flask, request, jsonify, render_template, flash, redirect, make_response, url_for
import time
from voda import voda 
import requests
import json
from flask_cors import CORS
import math
import random
CORS(voda)
application = voda

headers = {'authorization' : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJ3blZXWXRvNWp1cnNYZnJNMmRTMXQiLCJzdWIiOiI1ZTc0YzY5Y2M3ODljNDY1MmIzNzIwY2UiLCJncnAiOiI1ZTc0YzY5Y2M3ODljNDY1MmIzNzIwY2QiLCJ1c2VyIjoiaHUyMDIwLTAxIiwicmlnaHRzIjoxLjUsInNjb3BlcyI6WyJtb2RlbHNfZ2V0IiwibW9kZWxzX29uZV9nZXQiLCJtb2RlbHNfcG9zdCIsIm1vZGVsc19vbmVfcGF0Y2giLCJtb2RlbHNfb25lX2RlbGV0ZSIsIm1vZGVsc19hY2Nlc3NfcG9zdCIsIm1vZGVsc19hY2Nlc3NfZGVsZXRlIiwibW9kZWxzX29uZV9ub2Rlc19wb3N0IiwibW9kZWxzX29uZV9ub2Rlc19ub2RlX3BhdGNoIiwibW9kZWxzX29uZV9ub2Rlc19ub2RlX2RlbGV0ZSIsIm1vZGVsc19vbmVfYXJndW1lbnRzX2dldCIsIm1vZGVsc19vbmVfY29tbWFuZHNfZ2V0IiwibW9kZWxzX29uZV9ldmVudHNfZ2V0Iiwib2JqZWN0c19nZXQiLCJvYmplY3RzX29uZV9nZXQiLCJvYmplY3RzX3Bvc3QiLCJvYmplY3RzX29uZV9wYXRjaCIsIm9iamVjdHNfb25lX2RlbGV0ZSIsIm9iamVjdHNfYWNjZXNzX3Bvc3QiLCJvYmplY3RzX2FjY2Vzc19kZWxldGUiLCJvYmplY3RzX29uZV9wYWNrZXRzX2dldCIsIm9iamVjdHNfb25lX3BhY2tldHNfcGFja2V0aWRfZ2V0Iiwib2JqZWN0c19vbmVfbW9kZWxfcG9zdCIsIm9iamVjdHNfb25lX3Rhc2tfY2hhbmdlaWRfcG9zdCIsIm9iamVjdHNfb25lX3Rhc2tfY2VydF9wb3N0Iiwib2JqZWN0c19vbmVfdGFza19kZWxldGVfcG9zdCIsIm9iamVjdHNfb25lX3BhY2tldHNfcG9zdCIsImNvbW1hbmRzX3dlYl9vYmplY3RzX29uZV9jb21tYW5kc19nZXRfc3RhdGVfcG9zdCIsImNvbW1hbmRzX3dlYl9vYmplY3RzX29uZV9jb21tYW5kc19kaXNjb25uZWN0X3Bvc3QiLCJjb21tYW5kc193ZWJfb2JqZWN0c19vbmVfY29tbWFuZHNfcHVibGlzaF9wb3N0IiwiY29tbWFuZHNfd2ViX29iamVjdHNfb25lX2NvbW1hbmRzX3RleHRfY29tbWFuZF9wb3N0IiwiY29tbWFuZHNfd2ViX29iamVjdHNfb25lX2NvbW1hbmRzX3VwZGF0ZV9tb2RlbF9wb3N0IiwiY29tbWFuZHNfd2ViX29iamVjdHNfb25lX2NvbW1hbmRzX3NlcnZpY2VfcG9zdCIsImNvbW1hbmRzX3dlYl9vYmplY3RzX29uZV9jb21tYW5kc19pbmZvX3Bvc3QiLCJjb21tYW5kc193ZWJfb2JqZWN0c19vbmVfY29tbWFuZHNfY29udHJvbF9wb3N0IiwiY29tbWFuZHNfd2ViX29iamVjdHNfb25lX2NvbW1hbmRzX2Rhbmdlcl9wb3N0IiwiY29tbWFuZHNfd2ViX29iamVjdHNfb25lX2NvbW1hbmRzX2NvbW1hbmRfcG9zdCIsImxvZ2ljX2F1dG9tYXRvbnNfb25lX3N0YXR1c2VzX2dldCIsImxvZ2ljX29iamVjdHNfb25lX2F1dG9tYXRvbnNfYXV0b21hdG9uX2dldCIsImxvZ2ljX29iamVjdHNfb25lX2F1dG9tYXRvbnNfYXV0b21hdG9uX2xvZ3NfZ2V0IiwibG9naWNfb2JqZWN0c19vbmVfYXV0b21hdG9uc19hdXRvbWF0b25fc3RhcnRfcG9zdCIsImxvZ2ljX29iamVjdHNfb25lX2F1dG9tYXRvbnNfYXV0b21hdG9uX2VtaXRfcG9zdCIsImxvZ2ljX29iamVjdHNfb25lX2F1dG9tYXRvbnNfYXV0b21hdG9uX3N0b3BfcG9zdCIsImxvZ2ljX2F1dG9tYXRvbnNfZ2V0IiwibG9naWNfYXV0b21hdG9uc19vbmVfZ2V0IiwibG9naWNfYXV0b21hdG9uc19wb3N0IiwibG9naWNfYXV0b21hdG9uc19vbmVfcGF0Y2giLCJsb2dpY19hdXRvbWF0b25zX29uZV9kZWxldGUiLCJsb2dpY19hdXRvbWF0b25zX2FjY2Vzc19wb3N0IiwibG9naWNfYXV0b21hdG9uc19hY2Nlc3NfZGVsZXRlIiwibG9naWNfb2JqZWN0c19vbmVfY29udGFpbmVyc19nZXQiLCJsb2dpY19vYmplY3RzX29uZV9jb250YWluZXJzX3V1aWRfZ2V0IiwibG9naWNfb2JqZWN0c19vbmVfY29udGFpbmVyc191dWlkX3Bvc3QiLCJsb2dpY19vYmplY3RzX29uZV9jb250YWluZXJzX3V1aWRfZGVsZXRlIiwibG9naWNfb2JqZWN0c19vbmVfY29udGFpbmVyc191dWlkX3N0YXJ0X3Bvc3QiLCJsb2dpY19vYmplY3RzX29uZV9jb250YWluZXJzX3V1aWRfc3RvcF9wb3N0IiwibG9naWNfb2JqZWN0c19vbmVfZW1pdF9wb3N0IiwibG9naWNfb2JqZWN0c19vbmVfY29udGFpbmVyc191dWlkX2VtaXRfcG9zdCIsImxvZ2ljX29iamVjdHNfb25lX2F1dG9tYXRvbl90ZW1wbGF0ZWlkX3Bvc3QiLCJsb2dpY19hdXRvbWF0b25zX29uZV9kYXRhX2dldCIsImhhbmRsZXJzX2dldCIsImhhbmRsZXJzX29uZV9nZXQiLCJoYW5kbGVyc19wb3N0IiwiaGFuZGxlcnNfb25lX3BhdGNoIiwiaGFuZGxlcnNfb25lX2RlbGV0ZSIsImhhbmRsZXJzX2FjY2Vzc19wb3N0IiwiaGFuZGxlcnNfYWNjZXNzX2RlbGV0ZSIsImhhbmRsZXJzX3Rlc3RfY29kZV9wb3N0IiwiZHJpdmVyc19nZXQiLCJkcml2ZXJzX29uZV9nZXQiLCJkcml2ZXJzX3Bvc3QiLCJkcml2ZXJzX29uZV9wYXRjaCIsImRyaXZlcnNfb25lX2RlbGV0ZSIsImRyaXZlcnNfYWNjZXNzX3Bvc3QiLCJkcml2ZXJzX2FjY2Vzc19kZWxldGUiLCJsaW5rc19kcml2ZXJzX3RvX29iamVjdHNfcG9zdCIsImxpbmtzX2F1dG9tYXRvbnNfdG9fb2JqZWN0c19wb3N0IiwibGlua3NfYWN0aW9uc190b19vYmplY3RzX3Bvc3QiLCJnZW9mZW5jZXNfZ2V0IiwiZ2VvZmVuY2VzX29uZV9nZXQiLCJnZW9mZW5jZXNfcG9zdCIsImdlb2ZlbmNlc19vbmVfcGF0Y2giLCJnZW9mZW5jZXNfb25lX2RlbGV0ZSIsImdlb2ZlbmNlc19hY2Nlc3NfcG9zdCIsImdlb2ZlbmNlc19hY2Nlc3NfZGVsZXRlIiwiZ2VvZmVuY2VzX29iamVjdHNfb25lX2dlb2ZlbmNlc19nZXQiLCJnZW9mZW5jZXNfb2JqZWN0c19vbmVfZ2VvZmVuY2VzX2dlb2ZlbmNlX3dhdGNoX3Bvc3QiLCJnZW9mZW5jZXNfb2JqZWN0c19vbmVfZ2VvZmVuY2VzX2dlb2ZlbmNlX3Vud2F0Y2hfcG9zdCIsInNoYXBlc19nZXQiLCJzaGFwZXNfb25lX2dldCIsIm1lc3NhZ2VzX2dldCIsIm1lc3NhZ2VzX3Bvc3QiLCJtZXNzYWdlc19yZWFkX29uZV9wYXRjaCIsIm1lc3NhZ2VzX2NsZWFyX2RlbGV0ZSIsIm1lc3NhZ2VzX2hpc3RvcnlfZ2V0IiwibWVzc2FnZXNfaGlzdG9yeV9ib3VuZHNfZ2V0IiwiZmlsZXNfZ2V0IiwiZmlsZXNfb25lX2dldCIsImZpbGVzX3Bvc3QiLCJmaWxlc19vbmVfZGVsZXRlIiwiZ3JvdXBzX2dldCIsImdyb3Vwc19vbmVfZ2V0IiwidXNlcnNfb25lX3Rva2Vuc19nZXQiLCJ1c2Vyc190b2tlbnNfcG9zdCIsInVzZXJzX29uZV90b2tlbnNfdGVzdF9wb3N0IiwidXNlcnNfYmlsbGluZ19wcmljZV9jb25maWdfZ2V0IiwidXNlcnNfdG9rZW5zX2dldCIsImNvbW1vbl9nZW9fcmV2ZXJzZV9nZXQiLCJjb21tb25fZ2VvX3JvdXRlX2dldCIsImNvbmZpZ19jb25maWciLCJjb25zb2xlX2xlZ2FjeV9zZWNyZXRzX2dldCIsImNvbnNvbGVfbGVnYWN5X3NlY3JldHNfb25lX2dldCIsImxpbmtzX2xpbmtzIiwibW9kZWxzX3dlYl9tb2RlbHNfd2ViIiwibW9kZWxzX3YzX21vZGVscyIsIm1vZGVsc192M19lZGl0b3IiLCJvYmplY3RzX3ZpZXdfbWFwIiwib2JqZWN0c192aWV3X3RhYmxlIiwib2JqZWN0c19ib3RzX29iamVjdHNfb25lX2JvdF9zdGFydF9wb3N0Iiwib2JqZWN0c19ib3RzX29iamVjdHNfb25lX2JvdF9zdG9wX3Bvc3QiLCJvYmplY3RzX2JvdHNfb2JqZWN0c19vbmVfYm90X3N0YXRlX3Bvc3QiLCJvYmplY3RzX2JvdHNfb2JqZWN0c19vbmVfYm90X2NvbmZpZ19wb3N0Iiwib2JqZWN0c19ib3RzX29iamVjdHNfb25lX2JvdF9nZW9fc3RhcnRfcG9zdCIsIm9iamVjdHNfYm90c19vYmplY3RzX29uZV9ib3RfZ2VvX3BhdXNlX3Bvc3QiLCJvYmplY3RzX2JvdHNfb2JqZWN0c19vbmVfYm90X2dlb19zdG9wX3Bvc3QiLCJjaGFpbnNfdjJfZ2V0IiwiY2hhaW5zX3YyX29uZV9nZXQiLCJjaGFpbnNfb2JqZWN0c19jaGFpbnNfZ2V0IiwiY2hhaW5zX29iamVjdHNfb25lX2NoYWluc19nZXQiLCJjaGFpbnNfb2JqZWN0c19vbmVfY2hhaW5zX2NvbnRhaW5lcl9wb3N0IiwiY2hhaW5zX29iamVjdHNfb25lX2NoYWluc19jb250YWluZXJfcGF0Y2giLCJjaGFpbnNfb2JqZWN0c19vbmVfY2hhaW5zX2NvbnRhaW5lcl9kZWxldGUiLCJjaGFpbnNfb2JqZWN0c19vbmVfcHJvY2Vzc2luZ19zdGFydF9wb3N0IiwiY2hhaW5zX29iamVjdHNfb25lX3Byb2Nlc3Npbmdfc3RvcF9wb3N0IiwibG9naWNfdjJfbG9naWMiXSwiaWF0IjoxNTg0NzMxMTk0LCJleHAiOjE1ODcyNDM1OTl9.-kqJPe38yXeCPvV25rfcEYyIKiiS4z7T7pmxMJOLmuc', 'Content-Type' : 'application/json'}


voda.config['SECRET_KEY'] = 'you never never never now this a really really really really long secret key'

@voda.route('/obj', methods = ['GET', 'POST'])
def obj():
	objects = requests.get('https://sandbox.rightech.io/api/v1/objects', headers=headers)
	objects = json.loads(objects.text)
	'''
	for i in range(len(objects)):
		if objects[i]["model"] == "5e751f2e1690d772cb22ed2b":
			for j in range(len(objects[i]['state']['ventils'])):
				response = requests.get('https://sandbox.rightech.io/api/v1/objects/' + str(objects[i]['state']['ventils'][j]), headers=headers)
				res = json.loads(response.text)
				objects[i]['state']['ventils'][j] = {'lat' : res['state']['lat'], 'long' : res['state']['long']}
	for i in range(len(objects)):
		if objects[i]["model"] == "5e75c5861690d76c7825dda3":
			for j in range(len(objects[i]['state']['dat_truba'])):
				response = requests.get('https://sandbox.rightech.io/api/v1/objects/' + str(objects[i]['state']['dat_truba'][j]), headers=headers)
				res = json.loads(response.text)
				objects[i]['state']['dat_truba'][j] = {'lat' : res['state']['lat'], 'long' : res['state']['long']}
			for j in range(len(objects[i]['state']['dat_rez'])):
				response = requests.get('https://sandbox.rightech.io/api/v1/objects/' + str(objects[i]['state']['dat_rez'][j]), headers=headers)
				res = json.loads(response.text)
				objects[i]['state']['dat_rez'][j] = {'lat' : res['state']['lat'], 'long' : res['state']['long']}
	for i in range(len(objects)):
		if objects[i]["model"] == "5e7512bdbb5a430af39bf453":
			for j in range(len(objects[i]['state']['dat_truba'])):
				response = requests.get('https://sandbox.rightech.io/api/v1/objects/' + str(objects[i]['state']['dat_truba'][j]), headers=headers)
				res = json.loads(response.text)
				objects[i]['state']['dat_truba'][j] = {'lat' : res['state']['lat'], 'long' : res['state']['long']}
	for i in range(len(objects)):
		if objects[i]["model"] == "5e751e731690d739c822ea12":
			for j in range(len(objects[i]['state']['dat_truba'])):
				response = requests.get('https://sandbox.rightech.io/api/v1/objects/' + str(objects[i]['state']['dat_truba'][j]), headers=headers)
				res = json.loads(response.text)
				objects[i]['state']['dat_truba'][j] = {'lat' : res['state']['lat'], 'long' : res['state']['long']}
			if objects[i]['state']['dat_vent']:
				for j in range(len(objects[i]['state']['dat_vent'])):
					response = requests.get('https://sandbox.rightech.io/api/v1/objects/' + str(objects[i]['state']['dat_vent'][j]), headers=headers)
					res = json.loads(response.text)
					objects[i]['state']['dat_vent'][j] = {'lat' : res['state']['lat'], 'long' : res['state']['long']}
			#if objects[i]['state']['dat_house']:
				#for j in range(len(objects[i]['state']['dat_house'])):
					#response = requests.get('https://sandbox.rightech.io/api/v1/objects/' + str(objects[i]['state']['dat_house'][j]), headers=headers)
					#res = json.loads(response.text)
					#objects[i]['state']['dat_house'][j] = {'lat' : res['state']['lat'], 'long' : res['state']['long']}
	'''
	return jsonify(objects)

def sort_trub(trubi):
	trubi_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + trubi, headers=headers)
	trubi_obj = json.loads(trubi_obj.text)
	return trubi_obj['state']['id'][-1]
	


@voda.route('/water', methods = ['GET', 'POST'])
def water():
	everything_list = []
	all_water = 0
	all_water_list = []
	water_start = random.randint(1000,3000)
	objects = requests.post('http://sandbox.rightech.io/api/v1/objects', headers=headers)
	objects = json.loads(objects.text)
	for obj in objects:
		if obj['model'] == '5e751f2e1690d772cb22ed2b':
			water_prev = obj['state']['obiem']
			ids = obj["_id"]
			ventils = obj['state']['ventils']
			vent_dict = {}
			for vent in ventils:
				vent_dict[vent] = ''
			data = {"temperature" : 10, "lat" : 44.779932, "long" : 37.818833, 'lvl' : 80, 'ventils' : ventils, 'obiem': water_prev+ water_start }
			response = requests.post('https://dev.rightech.io/api/v1/objects/' + str(ids) + '/packets', headers=headers, json=data)
			hran_water_extend = water_start
			vent_count = len(ventils)
			#all_water = []
			all_water_vent = 0
			for i in range(vent_count):
				if i != vent_count-1:
					vent_id = ventils[i]
					vent_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + vent_id, headers=headers)
					vent_obj = json.loads(vent_obj.text)
					vent_water_prev = vent_obj['state']['obiem']
					
					vent_water_ext = random.uniform(water_start / (vent_count*2), water_start /vent_count)
					#data = vent_obj['state']
					chance = random.randint(1,10)
					if chance == 10:
						vent_water_ext *= random.uniform(0.5, 1.0) 
					
					#data['obiem'] = vent_water_prev + vent_water_ext
					data= {'obiem': vent_water_prev + vent_water_ext}
					#all_water_list.append(vent_water_ext)
					all_water_vent += vent_water_ext
					hran_water_extend -= vent_water_ext
					response = requests.post('https://dev.rightech.io/api/v1/objects/' + str(vent_id) + '/packets', headers=headers, json=data)
					house_list = data['dat_dom']
					house_count = len(house_list)
					vent_water_start = vent_water_ext
					all_water_house = 0
					
					house_water_list = [vent_water_ext]
					for j in range(house_count):
						if j != house_count -1:
							house_id = house_list[j]
							house_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + house_id, headers=headers)
							house_obj = json.loads(house_obj.text)
							house_water_prev = house_obj['state']['obiem']
							house_water_ext = random.uniform(vent_water_start / (house_count*2), vent_water_start /house_count)
							data = house_obj['state']
							chance = random.randint(1,10)
							if chance == 10:
								house_water_ext *= random.uniform(0.5, 1.0)
							
							if  vent_obj['state']['upr'] == False:
								house_water_ext = 0
										
							#data['obiem'] = house_water_prev + house_water_ext
							data= {'obiem': house_water_prev + house_water_ext}
							all_water_house += house_water_ext
							vent_water_start -= house_water_ext
							response = requests.post('https://dev.rightech.io/api/v1/objects/' + str(house_id) + '/packets', headers=headers, json=data)
						else:
							house_id = house_list[j]
							house_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + house_id, headers=headers)
							house_obj = json.loads(house_obj.text)
							house_water_prev = house_obj['state']['obiem']
							house_water_ext = vent_water_start
							data = house_obj['state']
							chance = random.randint(1,10)
							if chance == 10:
								house_water_ext *= random.uniform(0.5, 1.0)
							if  vent_obj['state']['upr'] == False:
								house_water_ext = 0
							#data['obiem'] = house_water_prev + house_water_ext
							data= {'obiem': house_water_prev + house_water_ext}
							all_water_house += house_water_ext
							response = requests.post('https://dev.rightech.io/api/v1/objects/' + str(house_id) + '/packets', headers=headers, json=data)
							#house_water_list.append(all_water_house)
							#all_water.append(house_water_list)
					
							
				else:
					vent_id = ventils[i]
					vent_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + vent_id, headers=headers)
					vent_obj = json.loads(vent_obj.text)
					vent_water_prev = vent_obj['state']['obiem']
					vent_water_ext = hran_water_extend
					#all_water_list.append(vent_water_ext)
					data = vent_obj['state']
					if chance == 10:
						vent_water_ext *= random.uniform(0.5, 1.0) 
					#data['obiem'] = vent_water_prev + vent_water_ext
					data= {'obiem': house_water_prev + house_water_ext}
					all_water_vent += vent_water_ext
					response = requests.post('https://dev.rightech.io/api/v1/objects/' + str(vent_id) + '/packets', headers=headers, json=data)
					house_list = data['dat_dom']
					house_count = len(house_list)
					vent_water_start = vent_water_ext
					all_water_house = 0
					house_water_list = [vent_water_ext]
					for j in range(house_count):
						if j != house_count -1:
							house_id = house_list[j]
							house_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + house_id, headers=headers)
							house_obj = json.loads(house_obj.text)
							house_water_prev = house_obj['state']['obiem']
							house_water_ext = random.uniform(vent_water_start / (house_count*2), vent_water_start /house_count)
							data = house_obj['state']
							chance = random.randint(1,10)
							if chance == 10:
								house_water_ext *= random.uniform(0.5, 1.0)
							#data['obiem'] = house_water_prev + house_water_ext
							if  vent_obj['state']['upr'] == False:
								house_water_ext = 0
							data= {'obiem': house_water_prev + house_water_ext}
							all_water_house += house_water_ext
							vent_water_start -= house_water_ext
							response = requests.post('https://dev.rightech.io/api/v1/objects/' + str(house_id) + '/packets', headers=headers, json=data)
						else:
							house_id = house_list[j]
							house_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + house_id, headers=headers)
							house_obj = json.loads(house_obj.text)
							house_water_prev = house_obj['state']['obiem']
							house_water_ext = vent_water_start
							data = house_obj['state']
							chance = random.randint(1,10)
							if chance == 10:
								house_water_ext *= random.uniform(0.5, 1.0)
							if  vent_obj['state']['upr'] == False:
								house_water_ext = 0
							#data['obiem'] = house_water_prev + house_water_ext
							data= {'obiem': house_water_prev + house_water_ext}
							all_water_house += house_water_ext
							response = requests.post('https://dev.rightech.io/api/v1/objects/' + str(house_id) + '/packets', headers=headers, json=data)
							house_water_list.append(all_water_house)
							all_water.append(house_water_list)
				if round(all_water_house) == round(vent_water_ext):
						vent_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + vent_id, headers=headers)
						vent_obj = json.loads(vent_obj.text)
						lat = vent_obj['state']['lat']
						lon = vent_obj['state']['long']
						house_dict = {'type' : 'vent', 'id' : vent_id,   'vent_result':'good', 'vent_loses': 0, 'lat' : lat, 'long':lon }
						#vent_result = 'good'
						#vent_loses = 0
						
						everything_list.append(house_dict)
				else:
						vent_obj = requests.get('https://sandbox.rightech.io/api/v1/objects/' + vent_id, headers=headers)
						vent_obj = json.loads(vent_obj.text)
						lat = vent_obj['state']['lat']
						lon = vent_obj['state']['long']
						house_dict = {'type' : 'vent', 'id' : vent_id, 'vent_result':'bad', 'vent_loses': round(vent_water_ext) - round(all_water_house), 'lat' : lat, 'long':lon}
						#vent_result = 'bad'
						#vent_loses = round(water_start) - round(all_water_vent)
						everything_list.append(house_dict)
	if round(all_water_vent) == round(water_start):
		hran_result = 'good'
		loses = 0
	else:
		hran_result = 'bad'
		loses = round(water_start) - round(all_water_vent)
	lat = vent_obj['state']['lat']
	lon = vent_obj['state']['long']
	hran_dict = {'id': ids, 'type': 'hran', 'hran_result' : hran_result, 'loses' : loses, 'all_water' : water_start, 'vent' : all_water_vent, 'lat': lat, 'long' : lon } 	
	everything_list.append(hran_dict)
	return jsonify({'response' : eval(str(everything_list))})
	#return jsonify(str({'hran_result' : hran_result, 'loses' : loses, 'all_water' : water_start, 'vent' : all_water_vent}))
	#return str(loses) + '____' + hran_vents + '______' +  str(water_start) + '______' + str(all_water_vent)

	
@voda.route('/', methods = ['GET', 'POST'])
def index():
	return "Привет UniversityHack2020"
	
	
@voda.route('/obj/<ID>', methods = ['GET', 'POST'])
def objecte(ID):
	ids = ID
	objects = requests.get('https://sandbox.rightech.io/api/v1/objects/' + str(ids), headers=headers)
	obj = json.loads(objects.text)
	return jsonify(obj)
	
@voda.route('/upr/<ID>/<upr>', methods = ['GET', 'POST'])
def upr(ID, upr):
	ids = ID
	data = {'upr' : str(upr)}
	response = requests.post('https://dev.rightech.io/api/v1/objects/' + str(ID) + '/packets', headers=headers, json=data)
	response = json.loads(response.text)
	return jsonify(response)
		
@voda.route('/journal/<ID>', methods = ['GET', 'POST'])
def jour(ID):
	ids = ID
	journal = requests.get('https://sandbox.rightech.io/api/v1/objects/5e7521331690d7280b22f576/packets?begin=1584735288280&end='+ str(int(time.time()) * 1000), headers=headers)
	jou = json.loads(journal.text)
	return jsonify(jou)



@voda.route('/api/obj_by_coord', methods = ['POST'])
def by_coord():
	data = request.data
	data = data.decode('utf-8')
	data = json.loads(data)
	lat_post = data['lat']
	lon_post = data['long']
	rad = data['radius']
	objects = requests.get('https://sandbox.rightech.io/api/v1/objects', headers=headers)
	obj_list = json.loads(objects.text)
	def ortodom(obj):
		#lat1, lat2, lon1, lon2
		lat1 = obj['state']['lat']  
		lon1 = obj['state']['long']
		
		lat2 = lat_post
		lon2 = lon_post
		radius = math.acos(math.sin(lat1) * math.sin(lat2) + math.cos(lat1)*math.cos(lat2)*math.cos(lon2-lon1))
		s = radius * 6371
		if s < rad:
			return True
		else:
			return False
	obj_list_fin = list(filter(ortodom, obj_list)) 
	return jsonify(obj_list_fin)
	
